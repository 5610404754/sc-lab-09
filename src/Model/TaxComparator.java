package Model;

import java.util.Comparator;

import Interface.Taxable;

public class TaxComparator implements Comparator<Taxable> {

	@Override
	public int compare(Taxable o1, Taxable o2) {
		double in1 = ((Taxable)o1).getTax(); 
		double in2 = ((Taxable)o2).getTax(); 
		if(in1>in2){
			return 1;
		}
		if(in1<in2){
			return -1;
		}
		return 0;
	
	}

	
}

