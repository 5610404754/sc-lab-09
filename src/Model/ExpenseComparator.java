package Model;

import java.util.Comparator;

public class ExpenseComparator implements Comparator {
	public int compare(Object ob1, Object ob2) {
		double in1 = ((Company)ob1).getExpense(); 
		double in2 = ((Company)ob2).getExpense(); 
		if(in1>in2){
			return 1;
		}
		if(in1<in2){
			return -1;
		}
		return 0;
	}
}
