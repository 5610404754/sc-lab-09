package Model;
import Interface.Taxable;

public class Company implements Taxable,Comparable<Company> {
	String name;
	double expense; 
	double income;
	public Company(String name,double income,double expense){
		this.name = name;
		this.income = income;
		this.expense = expense;
	}
	@Override
	public double getTax() {
		if (income > expense){
			return ((income-expense)*30)/100;
		}
		else {
			return 0;
		}				
	}
	public double getIncome(){
		return income;
	}
	
	public double getExpense(){
		return expense;
	}
	
	public double getProfit(){
		double profit = income - expense;
		return profit;
	}
	public String toString() {
		return "Company[name="+this.name+", income="+this.income+ ", expense= "+this.expense+", profit="+getProfit()+"]";
	}
	@Override
	public int compareTo(Company other) {
		if (this.income < other.income ) { return -1; }
		if (this.income > other.income ) { return 1;  }
		return 0;
	}
}
