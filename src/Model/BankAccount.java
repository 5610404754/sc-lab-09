package Model;

import Interface.Measurable;

public class BankAccount implements Measurable  {
	String name;
	double balance; 
	public BankAccount(String name,double balance){
		this.name = name;
		this.balance = balance;
	}
	
	@Override
	public double getMeasurable() {
		return balance;
	}


}
