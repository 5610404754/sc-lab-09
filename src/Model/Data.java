package Model;

import Interface.Measurable;

public class Data {
	public static double average(Measurable[] a){
		double sum = 0;
		for (Measurable m : a ){
			sum += m.getMeasurable(); ;
		}
		if (a.length > 0){
			return sum/a.length;
		}
		return 0;
	}
	
	public static Measurable measurable_min(Measurable m1, Measurable m2){
		if (m1.getMeasurable() < m2.getMeasurable()){
			return m1;
		}
		else {
			return m2;
		}		
	}
	public static Comparable compareToObject(Comparable c1, Comparable c2){
		int check = c1.compareTo(c2);
		if(check==1){
			return c1;
		}
		else{
			return c2;
		}
	}
}
