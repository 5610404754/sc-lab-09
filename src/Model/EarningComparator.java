package Model;

import java.util.Comparator;



public class EarningComparator implements Comparator<Company> {


	@Override
	public int compare(Company o1, Company o2) {
		double in1 = ((Company)o1).getIncome(); 
		double in2 = ((Company)o2).getIncome(); 
		if(in1>in2){
			return 1;
		}
		if(in1<in2){
			return -1;
		}
		return 0;
	}

	
}
