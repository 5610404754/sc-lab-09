package Model;

import java.util.Comparator;

public class ProfitComparator implements Comparator {
	public int compare(Object ob1, Object ob2) {
		double in1 = ((Company)ob1).getProfit(); 
		double in2 = ((Company)ob2).getProfit(); 
		if(in1>in2){
			return 1;
		}
		if(in1<in2){
			return -1;
		}
		return 0;
	}
}
