package Controller;
import Interface.Measurable;
import Model.BankAccount;
import Model.Company;
import Model.Country;
import Model.Data;
import Model.EarningComparator;
import Model.ExpenseComparator;
import Model.Person;
import Model.Product;
import Model.ProfitComparator;
import Model.TaxCalculater;
import Model.TaxComparator;
import Interface.Taxable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
public class ControlTest {

	public static void main(String[] args) {
		ControlTest test = new ControlTest();
		test.Testcase1();
		test.Testcase2();
		test.Testcase3();
		test.Testcase4();
		test.Testcase5();
		test.Testcase6();
		test.Testcase7();



	}
	
	public void Testcase1(){
		System.out.println("------------------------------");
		Measurable[] persons = new Measurable[3];
		persons[0] = new Person("Baitoey",158.0);
		persons[1] = new Person("OA",165.0);
		persons[2] = new Person("Taa",170.0);
		System.out.println("Person avg tall: "+Data.average(persons));
	}
	
	public void Testcase2(){
		System.out.println("------------------------------");
		Measurable[] bankvscountry = new Measurable[3];
		Person p1 = new Person("Baitoey",156.0);
		Person p2 = new Person("OA",175.0);
		BankAccount b1 = new BankAccount("Baitoey",20000);
		BankAccount b2 = new BankAccount("Baitong",15000);
		Country c1 = new Country("Thailand",543456324);
		Country c2 = new Country("Laos",3233222);
		
		bankvscountry[0] = Data.measurable_min(b1, b2);
		System.out.println("Bank Min: "+bankvscountry[0].getMeasurable());
		bankvscountry[1] = Data.measurable_min(c1, c2);
		System.out.println("Country Min: "+bankvscountry[1].getMeasurable());
		bankvscountry[2] = Data.measurable_min(p1, p2);
		System.out.println("Pesron Min: "+bankvscountry[2].getMeasurable());
		
	}
	public void Testcase3(){
		System.out.println("------------------------------");
		ArrayList<Taxable> persons = new ArrayList<Taxable>();
		ArrayList<Taxable> companies = new ArrayList<Taxable>();
		ArrayList<Taxable> products = new ArrayList<Taxable>();
		ArrayList<Taxable> all = new ArrayList<Taxable>();
		
		Person p1 = new Person("Baitoey",7300000);
		Person p2 = new Person("OA",450000);
		Person p3 = new Person("Taa",350000);
		persons.add(p1);
		persons.add(p2);
		persons.add(p3);
		
		Company c1 = new Company("A",1500000,400000);
		Company c2 = new Company("B",4508545,1322112);
		Company c3 = new Company("C",10000000,1234321);
		companies.add(c1);
		companies.add(c2);
		companies.add(c3);
		
		Product pd1 = new Product("Champoo",129);
		Product pd2 = new Product("Food",50);
		Product pd3 = new Product("BB-Cream",378);
		products.add(pd1);
		products.add(pd2);
		products.add(pd3);
		
		all.add(pd1);
		all.add(p1);
		all.add(c2);
		
		
		
		System.out.println("Persons sum tax: "+TaxCalculater.sum(persons));
		System.out.println("Companies sum tax: "+TaxCalculater.sum(companies));
		System.out.println("Produxt sum tax: "+TaxCalculater.sum(products));
		System.out.println("All sum tax: "+TaxCalculater.sum(all));
	}
	
	public void Testcase4(){
		System.out.println("------------------------------------------------");
		ArrayList<Person> per = new ArrayList<Person>();
		per.add(new Person("Thailand", 513000));
		per.add(new Person("Lao", 236800));
		Comparable dd = Data.compareToObject(per.get(0),per.get(1));
		
		System.out.println("(Before Sorting)");		
		for(int num=0;num<per.size();num++){
			System.out.println(per.get(num));
		}
		if(dd.equals(per.get(0))){
			System.out.println(per.get(0).toString()+" is greater than "+per.get(1).toString());
		}
		else{
			System.out.println(per.get(1).toString()+" is greater than "+per.get(0).toString());
		}		
		
		System.out.println("(After Sorting)");
		Collections.sort(per);
		
		for(int num=0;num<per.size();num++){
			System.out.println(per.get(num));
		}				
	}
	public void Testcase5(){
		System.out.println("------------------------------------------------");
		ArrayList<Product> product1 = new ArrayList<Product>();
		product1.add(new Product("Thailand", 513.4));
		product1.add(new Product("Lao", 236.4));
		Comparable ad = Data.compareToObject(product1.get(0),product1.get(1));
		System.out.println("(Before Sorting)");
		for(int number=0;number<product1.size();number++){
			System.out.println(product1.get(number));
		}	
		
		if(ad.equals(product1.get(0))){
			System.out.println(product1.get(0).toString()+" is greater than "+product1.get(1).toString());
		}
		else{
			System.out.println(product1.get(1).toString()+" is greater than "+product1.get(0).toString());
		}
		System.out.println("(After Sorting)");
		Collections.sort(product1);
		
		for(int number=0;number<product1.size();number++){
			System.out.println(product1.get(number));
		}			
	}
	
	public void Testcase6(){
		System.out.println("------------------------------------------------");
		ArrayList<Company> company1 = new ArrayList<Company>();
		company1.add(new Company("Thailand", 513.4,40));
		company1.add(new Company("Lao", 236.4,100));
		System.out.println("(Before Sorting)");
		EarningComparator ec = new EarningComparator();	
		int am = ec.compare(company1.get(0),company1.get(1));
		if(am==1){
			System.out.println((company1.get(0)).toString()+" has more earning than "+ (company1.get(1)).toString());
		}
		else{
			System.out.println((company1.get(1)).toString()+" has more earning than "+ (company1.get(0)).toString());
		}
		Collections.sort(company1,new EarningComparator());
		System.out.println("(After Sort with earning)");
		for(int number=0;number<company1.size();number++){
			System.out.println(company1.get(number));
		}
		
		System.out.println("\n");
		ArrayList<Company> company2 = new ArrayList<Company>();
		company2.add(new Company("Singapore", 400,40));
		company2.add(new Company("Indonesia", 300.5,200.5));
		System.out.println("(Before Sorting)");
		ExpenseComparator ex = new ExpenseComparator();
		int od = ex.compare(company1.get(0),company1.get(1));
		if(od==1){
			System.out.println((company2.get(0)).toString()+" has more expense than "+ (company2.get(1)).toString());
		}
		else{
			System.out.println((company2.get(1)).toString()+" has more expense than "+ (company2.get(0)).toString());
		}
		Collections.sort(company2,new EarningComparator());
		System.out.println("(After Sort with expense)");
		for(int number=0;number<company2.size();number++){
			System.out.println((company2.get(number)));
		}
		
		System.out.println("\n");
		ArrayList<Company> company3 = new ArrayList<Company>();
		company3.add(new Company("Thailand", 1000.3,400.3));
		company3.add(new Company("England", 236.4,100.4));
		System.out.println("(Before Sorting)");
		ProfitComparator pro = new ProfitComparator();
		int pd = pro.compare(company3.get(0), company3.get(1));
		if(pd==1){
			System.out.println((company3.get(0)).toString()+" has more profit than "+ (company3.get(1)).toString());
		}
		else{
			System.out.println((company3.get(1)).toString()+" has more profit than "+ (company3.get(0)).toString());
		}
		Collections.sort(company3,new ProfitComparator());
		System.out.println("(After Sort with profit)");
		for(int number=0;number<company2.size();number++){
			System.out.println((company2.get(number)));
		}				
	}
	
	public void Testcase7(){
		ArrayList<Taxable> all1 = new ArrayList<Taxable>();
		all1.add(new Company("Thailand", 1000.5,400.5));
		all1.add(new Person("baa", 100.5));
		all1.add(new Product("Food", 200.5));
		System.out.println("\n");

		System.out.println("(Before Sorting)");

//		Comparable lastt = Data.compareToObject(all1.get(0), all1.get(1));
		Collections.sort(all1,new TaxComparator());
		System.out.println(all1.get(0).toString()+" "+all1.get(1).toString()+" "+all1.get(2).toString());
		
//		if((all1.get(0)).getClass()>(all1.get(1)).getTax()){
//			if((all1.get(0)).getTax()>(all1.get(2)).getTax()){
//				System.out.println(all1.get(0).toString());
//			}
//		}
		
		
		
	}

	
	
}
