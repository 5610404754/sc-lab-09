package QuestionTwo;

import java.util.ArrayList;

public class PostOrderTraversal implements Traversal{
	ArrayList<String> list;
	
	public PostOrderTraversal(){
		list = new ArrayList<String>();
	}
	
	@Override
	public ArrayList<String> traverse(Node node) {
		if (node != null){
			traverse(node.getLeft());
			traverse(node.getRight());
			list.add(node.getValue());
		}
		return list;
	}

}
