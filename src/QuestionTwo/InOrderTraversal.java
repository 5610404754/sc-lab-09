package QuestionTwo;

import java.util.ArrayList;

public class InOrderTraversal implements Traversal{
	ArrayList<String> list;
	
	public InOrderTraversal(){
		list = new ArrayList<String>();
	}
	
	@Override
	public ArrayList<String> traverse(Node node) {
		if (node != null){
			traverse(node.getLeft());
			list.add(node.getValue());
			traverse(node.getRight());
		}
		return list;
	}

}
