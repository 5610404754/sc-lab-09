package QuestionTwo;

public class ReportConsole {
	public void display(Node root, Traversal traversal) {
		for (String n : traversal.traverse(root)) {
			System.out.print(n + " ");
		}
		System.out.println("");
	}
}
