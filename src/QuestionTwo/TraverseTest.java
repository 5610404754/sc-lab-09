package QuestionTwo;

public class TraverseTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Node h = new Node("H", null, null);
		Node e = new Node("E", null, null);
		Node c = new Node("C", null, null);
		Node i = new Node("I", h, null);
		Node d = new Node("D", c, e);
		Node a = new Node("A", null, null);
		Node g = new Node("G", null, i);
		Node b = new Node("B", a, d);
		Node f = new Node("F", b, g);

		ReportConsole report = new ReportConsole();

		System.out.println("PreorderTraversal: ");
		report.display(f, new PreOrderTraversal());

		System.out.println("InOrderTraversal: ");
		report.display(f, new InOrderTraversal());

		System.out.println("PostOrderTraversal: ");
		report.display(f, new PostOrderTraversal());
	}
}
