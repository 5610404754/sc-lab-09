package QuestionTwo;

import java.util.ArrayList;

public class PreOrderTraversal implements Traversal {
	ArrayList<String> list;
	
	public PreOrderTraversal(){
		list = new ArrayList<String>();
	}
	
	@Override
	public ArrayList<String> traverse(Node node) {
		if (node != null){
			String  n = node.getValue();
			list.add(n);
			traverse(node.getLeft());
			traverse(node.getRight());
		}
		return list;
	}

	
}
